<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Blog extends Model
{
    protected $fillable = [
        'category_id','title', 'content'
    ];
}