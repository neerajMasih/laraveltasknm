@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-3 Menus">
                            <ul>
                                <li> > <a href="{{ route('category.index') }}">Add Category</a> </li>
                                <li> >  <a href="{{ route('blogs.index') }}">Add Blogs</a> </li>                                
                            </ul>
                        </div>
                        <div class="col-9">
                            <h4>Blogs List</h4>
                             <table class="table table-bordered w3-table-all w3-card-4">
                <tr>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Category</th>
                    <th>Content</th>
                    <th width="280px">Action</th>
                </tr>
                @foreach ($blogs as $blog)
                <tr>
                    <td>{{ $blog->title }}</td>
                     <td>
                    @if(!empty($blog->image))
                            <img style="height:50px;" src="{{ Helper::ImageURL($blog->image)}}">
                            @else
                            <div class="fakeimg" style="height:50px;">Image</div>
                            @endif
                    </td>
                    <td>{{ $blog->category }}</td>
                    <td>{{ Helper::word_teaser($blog->content,15).'...' }}</td>
                    <td>
                        <form action="{{ route('blogs.destroy',$blog->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('blogs.show',$blog->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('blogs.edit',$blog->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
    </div>
</div>
@endsection
