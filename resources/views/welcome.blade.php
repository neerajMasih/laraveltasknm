<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Blogs</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">


        <link href="{{ asset('css/blog.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
            <div class="top-right links">
                @auth
                <a href="{{ url('/admin') }}">Dashbaord</a>
                @else
                <a href="{{ route('login') }}">Admin Login</a>


                @endauth
            </div>
            @endif            
            <div class="content">
                <div class="header">
                    &nbsp;
                    
                </div>
                <div class="row">
                    <div class="leftcolumn">
                        @if(!empty($blogs))
                        @foreach ($blogs as $blog)
                        <div class="card">
                            <h2> {{ $blog->title }} </h2>
                            @if(!empty($blog->image))
                            <img style="height:200px;" src="{{ Helper::ImageURL($blog->image)}}">
                            @else
                            <div class="fakeimg" style="height:200px;">Image</div>
                            @endif
                            <br />
                            <h5> {{ $blog->created_at }}</h5>
                            
                            <span> <b>Author :</b>  {{ Helper::getAuther($blog->user_id) }}</span>
                            
                            <p> {{ $blog->category }}</p>
                            <p>{{ $blog->content }}</p>
                        </div>
                        @endforeach
                        @endif                       
                    </div>
                    <div class="rightcolumn">
                       
                        <div class="card">
                            <h3> Blog Category </h3>                            
                                @if(!empty($category))
                                @foreach ($category as $cat)                                
                                <div class="fakeimg">{{ $cat->title }}</div><br>
                                @endforeach
                                @endif
                        </div>                      
                    </div>                    
                </div>
                <div class="footer">
                   &nbsp;
                </div>
            </div>
        </div>
    </body>
</html>
