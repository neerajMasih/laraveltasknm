<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */



Route::get('/', 'IndexController@show')->name('bloglist');
Route::resource('blogs', 'blogController');
Auth::routes();

Route::get('/facadeex', function() {
    return TestFacades::testingFacades();
});

Route::Group(['prefix' => env('BACKEND_PATH'), 'middleware' => ['auth', 'activation']], function () {
    Route::get('/admin', 'HomeController@index')->name('home');
    Route::resource('blogs', 'blogController');
    Route::resource('category', 'categoryController');


    // Clear Cache
    Route::get('/cache-clear', function () {
        Artisan::call('cache:clear');
        Artisan::call('view:clear');        
    })->name('cacheClear');
    
});



