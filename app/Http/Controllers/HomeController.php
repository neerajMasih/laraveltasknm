<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Blog;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blogs = Blog::select('categories.title as category','blogs.*')
                ->leftJoin('categories', 'blogs.category_id', '=', 'categories.id')
                ->orderby('id', 'desc')->get();
        return view('home',compact('blogs'));
    }
    public function show()
    {
        $blogs = Blog::select('categories.title as category','blogs.*')
                ->leftJoin('categories', 'blogs.category_id', '=', 'categories.id')
                ->orderby('id', 'desc')->get();
        return view('home',compact('blogs'));
    }
}
