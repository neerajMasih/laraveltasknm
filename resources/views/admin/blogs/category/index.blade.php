@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h5> Blog Category</h5>
                    </div>
                    <br />
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('category.create') }}"> Create New Category</a>
                    </div>
                    <br />
                   
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ url('/admin') }}"> Back</a>
                    </div>
                    <br />
                    
                </div>
            </div>
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
            <table class="table table-bordered w3-table-all w3-card-4">
                <tr>
                    <th>Title</th>
                   
                    <th width="280px">Action</th>
                </tr>
                @if(!empty($category))
                @foreach ($category as $cat)
                <tr>
                    <td>{{ $cat->title }}</td>        
                    <td>
                        <form action="{{ route('category.destroy',$cat->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('category.show',$cat->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('category.edit',$cat->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                @endif
            </table>
        </div></div>
    @endsection