<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Category;

//use Helper;

class IndexController extends Controller {

    private $ImgPath = 'uploads/users/';
    private $HTTP_status = 200;
    private $status = 0;
    private $message = '';
    private $response = array();
    private $Error = array();

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show() {
        $blogs = Blog::select('categories.title as category', 'blogs.*')
                        ->leftJoin('categories', 'blogs.category_id', '=', 'categories.id')
                        ->orderby('id', 'desc')->get();
        $category = Category::orderby('id', 'desc')->get();



        return view('welcome', compact('blogs', 'category'));
    }

    public function getUserdetails(Request $request) {
        if (empty($request->id)) {
            $this->Error['error'][] = 'The User ID field is required.';
        }
        if (!empty($this->Error)) {
            $this->Error = implode(",", $this->Error['error']);
            $this->status = 0;
            $this->message = $this->Error;
        } else {
            $res = User::where('id',$request->id)->where('status', '=', 1)->first();
            if ($res) {
                $this->response = $res;
                $this->message = 'Succ!';
                $this->status = 1;
            } else {
                $this->response = '';
                $this->message = 'ID is Invalid !';
                $this->status = 0;
            }
        }
        return response()->json(['status' => $this->status, 'message' => $this->message, 'response' => $this->response], $this->HTTP_status);
    }

}
