@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2> Show Blogs</h2>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('blogs.index') }}"> Back</a>
                    </div>
                     <br />
                </div>
            </div>
            <div class="row">
                <br />
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Title:</strong>
                        {{ $blog->title }}
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Title:</strong>
                        {{ Helper::getCategory($blog->category_id) }}
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Image:</strong>
                            @if(!empty($blog->image))
                            <img style="height:100px;" src="{{ Helper::ImageURL($blog->image)}}">
                            <input type="hidden" name="old_image" value="{{ $blog->image }}">
                            @else
                            <div class="fakeimg" style="height:100px;">Image</div>
                            @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Content:</strong>
                        {{ $blog->content }}
                    </div>
                </div>
            </div></div></div></div>
@endsection