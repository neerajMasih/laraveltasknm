<?php

namespace App\Http\Controllers;

use App\Category;
use App\Blog;
use Illuminate\Http\Request;
use Auth;
use Image;
use Helper;

class blogController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $blogs = Blog::select('categories.title as category', 'blogs.*')
                        ->leftJoin('categories', 'blogs.category_id', '=', 'categories.id')
                        ->orderby('id', 'desc')->get();
        return view('admin.blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $category = Category::orderby('id', 'desc')->get();
        return view('admin.blogs.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);
        $post = new Blog;
        $post->category_id = $request->category_id;
        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->slug = str_slug($request->slug);
        $post->content = Helper::checkEmpty($request->content);
        $filename = '';
        if (!empty($request->file('image'))) {
            $filename = Helper::uploadImage($request->file('image'));
        }
        $post->image = $filename;
        $post->save();
        return redirect()->route('blogs.index')
                        ->with('success', 'blogs created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog) {
        return view('admin.blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog) {
        $category = Category::orderby('id', 'desc')->get();
        return view('admin.blogs.edit', compact('blog', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);
        $post = Blog::find($id);
        $post->category_id = $request->category_id;
        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->slug = str_slug($request->slug);
        $post->content = Helper::checkEmpty($request->content);
        $filename = '';
        if (!empty($request->file('image'))) {
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('public/images/') . $filename;
            Image::make($image)->save($location);
        } else {
            $filename = $request->old_image;
        }
        $post->image = $filename;
        $post->save();
        return redirect()->route('blogs.index')
                        ->with('success', 'blogs updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog) {
        $blog->delete();
        return redirect()->route('blogs.index')
                        ->with('success', 'Blog deleted successfully');
    }

}
