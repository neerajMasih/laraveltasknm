<?php

// This class file to define all general functions

namespace App\Helpers;

use Auth;
use Session;
use App\User;
use App\Category;
use Image;
class Helper {    
    
    static function getCategory($id) {
        $title = '';
        if (!empty($id)) {
            $cat = Category::find($id);
            if (!empty($cat->title)) {
                $title = $cat->title;
            }
            return $title;
        } else {
            return false;
        }
    }

    static function getAuther($id) {
        $name = '';
        if (!empty($id)) {
            $user = User::find($id);
            if (!empty($user->name)) {
                $name = $user->name;
            }
            return $name;
        } else {
            return false;
        }
    }
    
     static function ImageURL($image) {        
        return asset('public/images/'.$image);       
     }

    static function uploadImage($image) {
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $location = public_path('public/images/') . $filename;
        Image::make($image)->save($location);
        return $filename;
    }

    static function word_teaser($string, $count) {
        $original_string = $string;
        $words = explode(' ', $original_string);

        if (count($words) > $count) {
            $words = array_slice($words, 0, $count);
            $string = implode(' ', $words);
        }
        return $string;
    }

    static function checkEmpty($val) {
        $res = '';
        if (!empty($val)) {
            if (is_numeric($val)) {
                $res = trim($val);
            } else {
                $res = ucfirst(trim($val));
            }
        }
        return $res;
    }

}
