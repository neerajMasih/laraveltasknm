@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Edit Blogs</h2>
                    </div>
                    <br />
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('blogs.index') }}"> Back</a>
                    </div>
                    <br />
                </div>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('blogs.update',$blog->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Category:</strong>
                            <select class="form-control" name="category_id">
                                <option value="">--Select -- </option>
                                @if(!empty($category))
                                @foreach ($category as $cat)
                                <option @if($blog->category_id==$cat->id) selected="selected" @endif value="{{ $cat->id }}"> {{ $cat->title }} </option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Title:</strong>
                            <input type="text" name="title" value="{{ $blog->title }}" class="form-control" placeholder="Name">
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Title:</strong>
                            <input type="file" name="image" class="form-control" placeholder="Image">
                            @if(!empty($blog->image))
                            <img style="height:100px;" src="{{ Helper::ImageURL($blog->image)}}">
                            <input type="hidden" name="old_image" value="{{ $blog->image }}">
                            @else
                            <div class="fakeimg" style="height:100px;">Image</div>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Content:</strong>
                            <textarea class="form-control" style="height:150px" name="content" placeholder="Detail">{{ $blog->content }}</textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection